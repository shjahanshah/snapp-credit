package com.snap.credit;

import com.snap.credit.domain.Role;
import com.snap.credit.domain.User;
import com.snap.credit.dto.request.UserChangePasswordRequestDTO;
import com.snap.credit.exception.role_service.user_service.RoleNotExistExceptionException;
import com.snap.credit.exception.user_service.UserCantPersistException;
import com.snap.credit.exception.user_service.UserChangePassWordEqualsException;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import com.snap.credit.repository.RoleRepository;
import com.snap.credit.repository.UserRepository;
import com.snap.credit.service.UserRoleServiceImpl;
import com.snap.credit.service.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.lenient;

@RunWith(MockitoJUnitRunner.class)
public class UserRoleServiceTests {
    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @InjectMocks
    private UserRoleServiceImpl userRoleService;
    private Role adminRole = Role.builder().id(1L).roleName("ADMIN").active(true).build();
    private User user = User.builder().id(1L).mobile("09123456789").fullName("admin").email(null)
            .password("admin").active(true).build();
    private User assignedRoleUser = User.builder().id(1L).mobile("09123456789").fullName("admin").email(null)
            .password("admin").active(true).roles(Collections.singletonList(adminRole)).build();
    private List<String> roles = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.userRoleService = new UserRoleServiceImpl(userRepository, roleRepository);
        roles.add(adminRole.getRoleName());
    }

    @Test
    public void assignUserRole() throws UserDoesNotExistException {
        Mockito.when(userRepository.findByMobile(user.getMobile())).thenReturn(user);
        Mockito.when(roleRepository.findByRoleName(adminRole.getRoleName())).thenReturn(adminRole);
        Mockito.when(userRepository.save(user)).thenReturn(assignedRoleUser);
        Assert.assertEquals(userRoleService.assignRole(user.getMobile(), roles), assignedRoleUser);
    }

    @Test(expected = UserDoesNotExistException.class)
    public void assignNullUserRole() throws UserDoesNotExistException {
        given(userRoleService.assignRole(user.getMobile(), roles)).willThrow(new UserDoesNotExistException(user.getMobile()));
    }
}
