package com.snap.credit;

import com.snap.credit.domain.User;
import com.snap.credit.domain.Wallet;
import com.snap.credit.dto.request.UserChangePasswordRequestDTO;
import com.snap.credit.exception.user_service.UserCantPersistException;
import com.snap.credit.exception.user_service.UserChangePassWordEqualsException;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import com.snap.credit.exception.wallet_service.WalletDoesNotExistException;
import com.snap.credit.exception.wallet_service.WalletExistForUserException;
import com.snap.credit.repository.UserRepository;
import com.snap.credit.repository.WalletRepository;
import com.snap.credit.service.UserServiceImpl;
import com.snap.credit.service.WalletServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.lenient;

@RunWith(MockitoJUnitRunner.class)
public class WalletServiceTests {
    @Mock
    private UserRepository userRepository;
    @Mock
    private WalletRepository walletRepository;
    @InjectMocks
    private WalletServiceImpl walletService;


    private User user = User.builder().id(1L).mobile("09123456789").fullName("admin").email(null)
            .password("admin").active(true).build();

    private Wallet wallet = Wallet.builder().walletId(1L).walletUser(user).build();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.walletService = new WalletServiceImpl(walletRepository);
    }

    @Test
    public void findAllWallets() {
        List<Wallet> wallets = new ArrayList<>(Arrays.asList(wallet));
        Mockito.when(walletRepository.findAll()).thenReturn(wallets);
        List<Wallet> list = walletService.allWallets();
        Assert.assertEquals(list.get(0), wallet);
        Assert.assertEquals(list.size(), 1);
    }

    @Test
    public void getWallet() throws WalletDoesNotExistException {
        lenient().when(walletRepository.findByWalletUser(user)).thenReturn(wallet);
        Assert.assertEquals(walletService.findWalletById(user), wallet);
    }

    @Test(expected = WalletDoesNotExistException.class)
    public void getWalletNotFound() throws  WalletDoesNotExistException {
        given(walletService.findWalletById(user)).willThrow(new WalletDoesNotExistException(user.getMobile()));
    }

    @Test
    public void addWallet() throws WalletExistForUserException {
        lenient().when(walletRepository.save(wallet)).thenReturn(wallet);
        Assert.assertEquals(walletService.addWallet(wallet), wallet);
    }

    @Test(expected = WalletExistForUserException.class)
    public void addWalletExistWallet() throws WalletExistForUserException {
        lenient().when(walletRepository.findByWalletIdAndWalletUser(wallet.getWalletId(), wallet.getWalletUser())).thenReturn(wallet);
        given(walletService.addWallet(wallet)).willThrow(new WalletExistForUserException(user.getMobile()));
    }
}
