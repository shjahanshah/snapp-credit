package com.snap.credit;

import com.snap.credit.domain.Role;
import com.snap.credit.exception.role_service.user_service.RoleNotExistExceptionException;
import com.snap.credit.repository.RoleRepository;
import com.snap.credit.service.RoleServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.lenient;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceTests {
    @Mock
    private RoleRepository roleRepository;
    @InjectMocks
    private RoleServiceImpl roleService;

    private Role adminRole = Role.builder().id(1L).roleName("ADMIN").active(true).build();
    private Role userRole = Role.builder().id(3L).roleName("USER").active(true).build();
    private Role cabRole = Role.builder().id(4L).roleName("SNAPP_CAB").active(true).build();
    private Role foodRole = Role.builder().id(5L).roleName("SNAPP_FOOD").active(true).build();

    private Role deactiveAdminRole = Role.builder().id(1L).roleName("ADMIN").active(false).build();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.roleService = new RoleServiceImpl(roleRepository);
    }

    @Test
    public void findAllRoles() {
        List<Role> roles = new ArrayList<>(Arrays.asList(adminRole, userRole, cabRole, foodRole));
        Mockito.when(roleRepository.findAll()).thenReturn(roles);

        List<Role> list = roleService.allRole();
        Assert.assertEquals(list.size(), roles.size());
        Assert.assertEquals(list.get(0), adminRole);
        Assert.assertNotEquals(list.get(2), foodRole);
    }

    @Test
    public void activateAdminRole() throws RoleNotExistExceptionException {
        Mockito.when(roleRepository.findByRoleName("ADMIN")).thenReturn(adminRole);
        lenient().when(roleRepository.save(any(Role.class))).thenReturn(adminRole);
        Assert.assertEquals(roleService.activateRole("ADMIN"), deactiveAdminRole);
    }

    @Test(expected = RoleNotExistExceptionException.class)
    public void activateWithOutRoleTest() throws RoleNotExistExceptionException {
        Assert.assertEquals(roleRepository.findAll().size(), 0);
        given(roleService.activateRole("MYQDMIN")).willThrow(new RoleNotExistExceptionException("MYQDMIN"));
    }
}
