package com.snap.credit;

import com.snap.credit.domain.SnappAccount;
import com.snap.credit.domain.User;
import com.snap.credit.domain.Wallet;
import com.snap.credit.dto.request.UserChangePasswordRequestDTO;
import com.snap.credit.enums.SnappVenture;
import com.snap.credit.exception.account_service.wallet_service.AccountCantPersistException;
import com.snap.credit.exception.account_service.wallet_service.AccountDoesNotExistException;
import com.snap.credit.exception.user_service.UserCantPersistException;
import com.snap.credit.exception.user_service.UserChangePassWordEqualsException;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import com.snap.credit.repository.SnappAccountRepository;
import com.snap.credit.repository.UserRepository;
import com.snap.credit.service.SnappAccountServiceImpl;
import com.snap.credit.service.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.lenient;

@RunWith(MockitoJUnitRunner.class)
public class SnappAccountServiceTests {
    @Mock
    private UserRepository userRepository;
    @Mock
    private SnappAccountRepository snappAccountRepository;
    @InjectMocks
    private SnappAccountServiceImpl snappAccountService;

    private User user = User.builder().id(1L).mobile("09123456789").fullName("admin").email(null)
            .password("admin").active(true).build();
    private Wallet wallet = Wallet.builder().walletUser(user).walletId(1L).build();

    private SnappAccount snappAccount = SnappAccount.builder().userAccount(user).balance(1500).venture(SnappVenture.SNAPP_CAB).id(1L).walletHolder(wallet).build();
    private SnappAccount snappEditedAccount = SnappAccount.builder().userAccount(user).balance(5800).venture(SnappVenture.SNAPP_MARKET).id(1L).walletHolder(wallet).build();


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.snappAccountService = new SnappAccountServiceImpl(snappAccountRepository);
    }

    @Test
    public void findAllAccount() {
        List<SnappAccount> accounts = new ArrayList<>(Collections.singletonList(snappAccount));
        Mockito.when(snappAccountRepository.findAll()).thenReturn(accounts);
        List<SnappAccount> list = snappAccountService.allAccount();
        Assert.assertEquals(list.get(0), snappAccount);
        Assert.assertEquals(list.size(), 1);
    }

    @Test
    public void findAllAccountByPhone() {
        List<SnappAccount> accounts = new ArrayList<>(Collections.singletonList(snappAccount));
        Mockito.when(snappAccountRepository.findAllByUserAccount_Mobile(snappAccount.getUserAccount().getMobile())).thenReturn(accounts);
        List<SnappAccount> list = snappAccountService.allAccountsByPhone(snappAccount.getUserAccount().getMobile());
        Assert.assertEquals(list.get(0), snappAccount);
        Assert.assertEquals(list.size(), 1);
    }

    @Test
    public void findAllAccountByVenture() {
        List<SnappAccount> accounts = new ArrayList<>(Collections.singletonList(snappAccount));
        Mockito.when(snappAccountRepository.findAllByVenture(snappAccount.getVenture())).thenReturn(accounts);
        List<SnappAccount> list = snappAccountService.allBalanceByVenture(snappAccount.getVenture());
        Assert.assertEquals(list.get(0), snappAccount);
        Assert.assertEquals(list.size(), 1);
    }

    @Test
    public void saveNewUser() throws AccountCantPersistException {
        lenient().when(snappAccountRepository.save(any(SnappAccount.class))).thenReturn(snappAccount);
        Assert.assertEquals(snappAccountService.addAccount(snappAccount), snappAccount);
    }

    @Test(expected = AccountCantPersistException.class)
    public void saveNullUser() throws AccountCantPersistException {
        given(snappAccountService.addAccount(snappAccount)).willThrow(new AccountCantPersistException(user.getMobile()));
    }

    @Test
    public void editUser() throws AccountDoesNotExistException, AccountCantPersistException {
        lenient().when(snappAccountRepository.findByUserAccount(user)).thenReturn(snappAccount);
        lenient().when(snappAccountRepository.save(snappAccount)).thenReturn(snappEditedAccount);
        Assert.assertEquals(snappAccountService.editAccount(snappEditedAccount, user), snappEditedAccount);
    }

    @Test(expected = AccountDoesNotExistException.class)
    public void editNullUser() throws  AccountDoesNotExistException, AccountCantPersistException {
        given(snappAccountService.editAccount(snappEditedAccount, user)).willThrow(new AccountDoesNotExistException(user.getMobile()));
    }
}
