package com.snap.credit;

import com.snap.credit.domain.User;
import com.snap.credit.dto.request.UserChangePasswordRequestDTO;
import com.snap.credit.exception.user_service.UserCantPersistException;
import com.snap.credit.exception.user_service.UserChangePassWordEqualsException;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import com.snap.credit.repository.UserRepository;
import com.snap.credit.service.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.lenient;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserServiceImpl userService;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private User user = User.builder().id(1L).mobile("09123456789").fullName("admin").email(null)
            .password("admin").active(true).build();
    private User user_2 = User.builder().id(2L).mobile("09124065594").fullName("shahrooz jahanshah")
            .email(null).password("user").active(true).build();
    private User user_3 = User.builder().id(3L).mobile("09124065595").fullName("shahrooz jahanshah")
            .email(null).password("user").active(true).build();

    private User deactive_user_2 = User.builder().id(2L).mobile("09124065594").fullName("shahrooz jahanshah")
            .email(null).password("user").active(false).build();
    private User edited_user_2 = User.builder().id(2L).mobile("09124065594").fullName("shahrooz jahanbakhsh")
            .email(null).password("user").active(true).build();

    private UserChangePasswordRequestDTO changePasswordDto = UserChangePasswordRequestDTO.builder().id(1L)
            .mobile("09123456789").oldPassword("admin").newPassword("new_admin").build();
    private UserChangePasswordRequestDTO changeSameUserPasswordDto = UserChangePasswordRequestDTO.builder().id(1L)
            .mobile("09123456789").oldPassword("admin").newPassword("admin").build();


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.userService = new UserServiceImpl(userRepository, passwordEncoder);
    }

    @Test
    public void findAllUser() {
        List<User> users = new ArrayList<>(Arrays.asList(user, user_2, user_3));
        Mockito.when(userRepository.findAll()).thenReturn(users);
        List<User> list = userService.allUser();
        Assert.assertEquals(list.get(0), user);
        Assert.assertEquals(list.size(), 3);
    }

    @Test
    public void saveNewUser() throws UserCantPersistException {
        lenient().when(userRepository.save(any(User.class))).thenReturn(user);
        Assert.assertEquals(userService.saveUser(user), user);
    }

    @Test(expected = UserCantPersistException.class)
    public void saveNullUser() throws UserCantPersistException {
        given(userService.saveUser(user)).willThrow(new UserCantPersistException(user.getMobile()));
    }

    @Test
    public void getUser() throws UserDoesNotExistException {
        lenient().when(userRepository.findByMobile(user.getMobile())).thenReturn(user);
        Assert.assertEquals(userService.getUser(user.getMobile()), user);
    }

    @Test(expected = UserDoesNotExistException.class)
    public void getUserNotFound() throws UserDoesNotExistException {
        given(userService.getUser(user.getMobile())).willThrow(new UserDoesNotExistException(user.getMobile()));
    }

    @Test
    public void editUser() throws UserDoesNotExistException {
        lenient().when(userRepository.findByMobile(user_2.getMobile())).thenReturn(user_2);
        lenient().when(userRepository.save(any())).thenReturn(edited_user_2);
        Assert.assertEquals(userService.editUser(edited_user_2), edited_user_2);
    }

    @Test(expected = UserDoesNotExistException.class)
    public void editNullUser() throws UserDoesNotExistException {
        given(userService.editUser(user)).willThrow(new UserCantPersistException(user.getMobile()));
    }

    @Test
    public void activateUser() throws UserDoesNotExistException {
        lenient().when(userRepository.findByMobile(user_2.getMobile())).thenReturn(user_2);
        lenient().when(userRepository.save(any())).thenReturn(deactive_user_2);
        Assert.assertEquals(userService.activateUser(user_2.getMobile()), deactive_user_2);
    }

    @Test(expected = UserDoesNotExistException.class)
    public void activateNotExistUser() throws UserDoesNotExistException {
        given(userService.activateUser(user.getMobile())).willThrow(new UserCantPersistException(user.getMobile()));
    }

    @Test
    public void changePassword() throws UserDoesNotExistException, UserChangePassWordEqualsException {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        lenient().when(userRepository.findByMobile(changeSameUserPasswordDto.getMobile())).thenReturn(user);
        Assert.assertEquals(userService.changePassword(changePasswordDto), true);
    }

    @Test(expected = UserDoesNotExistException.class)
    public void changeNullUserPassword() throws UserDoesNotExistException, UserChangePassWordEqualsException {
        given(userService.changePassword(changePasswordDto)).willThrow(new UserDoesNotExistException(user.getMobile()));
    }

    @Test(expected = UserChangePassWordEqualsException.class)
    public void changeSameUserPassword() throws UserChangePassWordEqualsException, UserDoesNotExistException {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        lenient().when(userRepository.findByMobile(changeSameUserPasswordDto.getMobile())).thenReturn(user);
        given(userService.changePassword(changeSameUserPasswordDto)).willThrow(new UserChangePassWordEqualsException(changeSameUserPasswordDto.getMobile()));
    }
}
