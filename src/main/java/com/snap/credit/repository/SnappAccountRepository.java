package com.snap.credit.repository;

import com.snap.credit.domain.SnappAccount;
import com.snap.credit.domain.User;
import com.snap.credit.enums.SnappVenture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public interface SnappAccountRepository extends JpaRepository<SnappAccount, Long> {
    List<SnappAccount> findAllByVenture(SnappVenture venture);

    List<SnappAccount> findAllByUserAccount_Mobile(@NotNull String userAccount_mobile);

    SnappAccount findByUserAccount(User userAccount);

}