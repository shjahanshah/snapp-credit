package com.snap.credit.repository;

import com.snap.credit.domain.User;
import com.snap.credit.domain.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {
    Wallet findByWalletIdAndWalletUser(Long walletId, User walletUser);

    Wallet findByWalletUser(User walletUser);
}