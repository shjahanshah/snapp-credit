package com.snap.credit.rest;

import com.snap.credit.decorator.RoleDecorator;
import com.snap.credit.dto.response.RoleResponseDTO;
import com.snap.credit.dto.response.UserResponseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/role/")
@Api(value = "/role", description = "role Api", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoleRestController {

    private final RoleDecorator roleDecorator;

    public RoleRestController(RoleDecorator roleDecorator) {
        this.roleDecorator = roleDecorator;
    }

    //   all role
    @GetMapping("/all")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "all users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<List<RoleResponseDTO>> allRoles() throws Exception {
        return roleDecorator.allRoles();
    }
    //    activate role
    @PatchMapping("/activate-role/{roleName}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "edit role")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "role created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "role not found")
    })
    public ResponseEntity<?> activateRole(@PathVariable String roleName) throws Exception {
        return roleDecorator.activateRole(roleName);
    }
}
