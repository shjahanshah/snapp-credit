package com.snap.credit.rest;

import com.snap.credit.decorator.WalletDecorator;
import com.snap.credit.dto.request.WalletRequestDTO;
import com.snap.credit.dto.response.RoleResponseDTO;
import com.snap.credit.dto.response.WalletResponseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/wallet")
@Api(value = "/wallet", description = "wallet Api", produces = MediaType.APPLICATION_JSON_VALUE)
public class WalletRestController {

    private final WalletDecorator walletDecorator;

    public WalletRestController(WalletDecorator walletDecorator) {
        this.walletDecorator = walletDecorator;
    }

    //   all wallet
    @GetMapping("/all")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "all wallets")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "wallet created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "wallet not found")
    })
    public ResponseEntity<List<WalletResponseDTO>> allWallets() throws Exception {
        return walletDecorator.allWallets();
    }

    //    add wallet
    @PostMapping("/add-wallet")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "activate wallet")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "wallet created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "wallet not found")
    })
    public ResponseEntity<?> addWallet(@RequestBody WalletRequestDTO dto) throws Exception {
        return walletDecorator.addWallet(dto);
    }
}
