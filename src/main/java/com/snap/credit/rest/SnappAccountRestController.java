package com.snap.credit.rest;

import com.snap.credit.decorator.AccountDecorator;
import com.snap.credit.dto.request.AccountRequestDTO;
import com.snap.credit.dto.response.AccountResponseDTO;
import com.snap.credit.enums.SnappVenture;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/account")
@Api(value = "/account", description = "account Api", produces = MediaType.APPLICATION_JSON_VALUE)
public class SnappAccountRestController {

    private final AccountDecorator accountDecorator;

    public SnappAccountRestController(AccountDecorator accountDecorator) {
        this.accountDecorator = accountDecorator;
    }

    //   all Account
    @GetMapping("/all")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "all users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<List<AccountResponseDTO>> allAccounts() {
        return accountDecorator.allAccount();
    }

    //   all Account by phone
    @GetMapping("/all/{phone}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "all users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<List<AccountResponseDTO>> allAccountsByPhone(@PathVariable String phone) {
        return accountDecorator.allAccountsByPhone(phone);
    }

    //   add Account
    @PostMapping("/add")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "add new user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<?> addAccount(@RequestBody @Valid AccountRequestDTO dto) {
        return accountDecorator.addAccount(dto);
    }

    //   edit Account
    @PutMapping("/edit")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "edit user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<?> editAccount(@RequestBody @Valid AccountRequestDTO dto) {
        return accountDecorator.editAccount(dto);
    }

    //   get balance by venture
    @GetMapping("/all/balance/{ventureName}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "all users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<List<AccountResponseDTO>> allBalanceByVenture(@PathVariable SnappVenture ventureName) {
        return accountDecorator.allBalanceByVenture(ventureName);
    }

}
