package com.snap.credit.rest;

import com.snap.credit.decorator.UserDecorator;
import com.snap.credit.dto.request.UserChangePasswordRequestDTO;
import com.snap.credit.dto.request.UserRequestDTO;
import com.snap.credit.dto.response.UserResponseDTO;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/user")
@Api(value = "/user", description = "User Api", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRestController {
    private final UserDecorator userDecorator;

    public UserRestController(UserDecorator userDecorator) {
        this.userDecorator = userDecorator;
    }

    @GetMapping("/all")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "all users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<List<UserResponseDTO>> allUsers() throws Exception {
        return userDecorator.allUser();
    }
    @GetMapping("/{userName}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "all users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<?> getUser(@PathVariable String username) throws Exception {
        return userDecorator.getUser(username);
    }

    @PostMapping("/add")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "add new user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<?> addUser(@RequestBody @Valid UserRequestDTO dto) throws Exception {
        return userDecorator.addUser(dto);
    }

    // edit user
    @PutMapping("/edit")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "edit user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<?> editUser(@RequestBody @Valid UserRequestDTO dto) throws Exception {
        return userDecorator.editUser(dto);
    }
    //    change password
    @PutMapping("/change-password")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "edit user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<?> changePassword(@RequestBody @Valid UserChangePasswordRequestDTO dto) throws Exception {
        return userDecorator.changePassword(dto);
    }
    //    activate user
    @PatchMapping("/activate-user/{phone}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "edit user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<?> activateUser(@PathVariable String phone) throws Exception {
        return userDecorator.activateUser(phone);
    }
}
