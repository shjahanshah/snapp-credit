package com.snap.credit.rest;

import com.snap.credit.decorator.UserRoleDecorator;
import com.snap.credit.dto.request.UserRoleAssignRequestDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/user-role")
@Api(value = "/user-role", description = "User Role Api", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRoleRestController {
    private final UserRoleDecorator userRoleDecorator;

    public UserRoleRestController(UserRoleDecorator userRoleDecorator) {
        this.userRoleDecorator = userRoleDecorator;
    }

    //    assign role
    @PutMapping("/assign-role")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "edit user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user created"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "user not found")
    })
    public ResponseEntity<?> assignRole(@RequestBody @Valid UserRoleAssignRequestDTO dto) throws Exception {
        return userRoleDecorator.assignRole(dto);
    }
}
