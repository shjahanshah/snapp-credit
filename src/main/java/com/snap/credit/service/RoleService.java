package com.snap.credit.service;

import com.snap.credit.domain.Role;
import com.snap.credit.exception.role_service.user_service.RoleNotExistExceptionException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService {
/*    //save new role
    Role saveRole(Role user);

    //get role by role name
    Role getRole(String roleName);*/

    //show all roles
    List<Role> allRole();

    Role activateRole(String roleName) throws RoleNotExistExceptionException;

}
