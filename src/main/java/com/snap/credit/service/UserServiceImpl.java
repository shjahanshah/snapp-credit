package com.snap.credit.service;

import com.snap.credit.domain.User;
import com.snap.credit.dto.request.UserChangePasswordRequestDTO;
import com.snap.credit.exception.user_service.UserCantPersistException;
import com.snap.credit.exception.user_service.UserChangePassWordEqualsException;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import com.snap.credit.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * save new User
     **/
    @Override
    public User saveUser(User user) throws UserCantPersistException {
        try {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            User saveUser = userRepository.save(user);
            if (saveUser == null) {
                throw new UserCantPersistException(user.getMobile());
            }
            logger.info("user saved!: {}", saveUser);
            return saveUser;
        } catch (Exception e) {
            throw new UserCantPersistException(user.getMobile());
        }
    }

    /**
     * get user by user name
     **/
    @Override
    public User getUser(String username) throws UserDoesNotExistException {
        User user = userRepository.findByMobile(username);
        if (user == null) {
            logger.info("user not fount with: {}", username);
            throw new UserDoesNotExistException(username);
        }
        logger.info("user by username is: {}", user);
        return user;
    }

    /**
     * show all users
     **/
    @Override
    public List<User> allUser() {
        return userRepository.findAll();
    }

    @Override
    public User editUser(User user) throws UserDoesNotExistException {
        User editUser = userRepository.findByMobile(user.getMobile());
        if (editUser == null) {
            throw new UserDoesNotExistException(user.getMobile());
        }
        editUser.setEmail(user.getEmail());
        editUser.setFullName(user.getFullName());
        editUser.setMobile(user.getMobile());
        editUser = userRepository.save(editUser);
        return editUser;
    }

    @Override
    public User activateUser(String phone) throws UserDoesNotExistException {
        User user = userRepository.findByMobile(phone);
        if (user == null) {
            throw new UserDoesNotExistException(phone);
        }
        user.setActive(!user.getActive());
        user = userRepository.save(user);
        return user;
    }

    @Override
    public Boolean changePassword(UserChangePasswordRequestDTO dto) throws UserDoesNotExistException, UserChangePassWordEqualsException {
        User user = userRepository.findByMobile(dto.getMobile());
        if (user == null) {
            throw new UserDoesNotExistException(dto.getMobile());
        }
        String newPasswordEncode = passwordEncoder.encode(dto.getNewPassword());

        if (passwordEncoder.matches(dto.getNewPassword(), user.getPassword())) {
            throw new UserChangePassWordEqualsException(dto.getMobile());
        }
        user.setPassword(newPasswordEncode);
        userRepository.save(user);
        return true;
    }
}
