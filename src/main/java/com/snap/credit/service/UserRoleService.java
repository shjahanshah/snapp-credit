package com.snap.credit.service;

import com.snap.credit.domain.User;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserRoleService {
    User assignRole(String userName, List<String> roleName) throws UserDoesNotExistException;
}
