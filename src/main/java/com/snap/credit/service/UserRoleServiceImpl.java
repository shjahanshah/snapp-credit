package com.snap.credit.service;

import com.snap.credit.domain.Role;
import com.snap.credit.domain.User;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import com.snap.credit.repository.RoleRepository;
import com.snap.credit.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {
    private Logger logger = LoggerFactory.getLogger(UserRoleServiceImpl.class);
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public UserRoleServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    /**
     * assign role to user
     **/
    @Override
    public User assignRole(String userName, List<String> roles) throws UserDoesNotExistException {
        User user = userRepository.findByMobile(userName);
        if (user == null) {
            throw new UserDoesNotExistException(userName);
        }
        roles.forEach(roleName -> {
            Role role = roleRepository.findByRoleName(roleName);
            user.setRoles(Collections.singletonList(role));
            User assignRole = userRepository.save(user);
            logger.info("user assigned the role saved!: {}", assignRole);
        });
        return user;
    }
}
