package com.snap.credit.service;

import com.snap.credit.domain.Role;
import com.snap.credit.exception.role_service.user_service.RoleNotExistExceptionException;
import com.snap.credit.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    public static Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);
    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

/*    @Override
    public Role saveRole(Role role) {
        Role saveRole = roleRepository.save(role);
        LOGGER.info("role saved!: {}", saveRole);
        return saveRole;
    }

    @Override
    public Role getRole(String roleName) {
        Role role = roleRepository.findByRoleName(roleName);
        LOGGER.info("role by roleName is: {}", role);
        return role;
    }*/

    @Override
    public List<Role> allRole() {
        return roleRepository.findAll();
    }

    @Override
    public Role activateRole(String roleName) throws RoleNotExistExceptionException {
        Role role = roleRepository.findByRoleName(roleName);
        if (role == null) {
            throw new RoleNotExistExceptionException(roleName);
        }
        role.setActive(!role.getActive());
        Role update = roleRepository.save(role);
        logger.info("role:{} is active:{}", roleName, update.getActive());
        return update;
    }
}
