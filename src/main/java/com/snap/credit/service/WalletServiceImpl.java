package com.snap.credit.service;

import com.snap.credit.domain.User;
import com.snap.credit.domain.Wallet;
import com.snap.credit.exception.wallet_service.WalletDoesNotExistException;
import com.snap.credit.exception.wallet_service.WalletExistForUserException;
import com.snap.credit.repository.WalletRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WalletServiceImpl implements WalletService {
    private Logger logger = LoggerFactory.getLogger(WalletServiceImpl.class);

    private final WalletRepository walletRepository;

    public WalletServiceImpl(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }


    @Override
    public List<Wallet> allWallets() {
        return walletRepository.findAll();
    }


    @Override
    public Wallet addWallet(Wallet wallet) throws WalletExistForUserException {
        Wallet walletUser = walletRepository.findByWalletIdAndWalletUser(wallet.getWalletId(), wallet.getWalletUser());
        if (walletUser != null) {
            throw new WalletExistForUserException(wallet.getWalletUser().getMobile());
        }
        wallet = walletRepository.save(wallet);
        return wallet;
    }

    @Override
    public Wallet findWalletById(User user) throws WalletDoesNotExistException {
        Wallet walletUser = walletRepository.findByWalletUser(user);
        if (walletUser == null) {
            logger.info("wallet not exist for phone:{}", user.getMobile());
            throw new WalletDoesNotExistException(user.getMobile());
        }
        logger.info("wallet exist for phone:{}", user.getMobile());
        return walletUser;
    }
}
