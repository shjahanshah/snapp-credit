package com.snap.credit.service;

import com.snap.credit.domain.SnappAccount;
import com.snap.credit.domain.User;
import com.snap.credit.enums.SnappVenture;
import com.snap.credit.exception.account_service.wallet_service.AccountCantPersistException;
import com.snap.credit.exception.account_service.wallet_service.AccountDoesNotExistException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SnappAccountService {

    List<SnappAccount> allAccount();

    List<SnappAccount> allBalanceByVenture(SnappVenture ventureName);

    SnappAccount editAccount(SnappAccount account, User user) throws AccountDoesNotExistException, AccountCantPersistException;

    SnappAccount addAccount(SnappAccount dto) throws AccountCantPersistException;

    List<SnappAccount> allAccountsByPhone(String phone);
}
