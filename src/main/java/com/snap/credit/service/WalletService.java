package com.snap.credit.service;

import com.snap.credit.domain.User;
import com.snap.credit.domain.Wallet;
import com.snap.credit.exception.wallet_service.WalletDoesNotExistException;
import com.snap.credit.exception.wallet_service.WalletExistForUserException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WalletService {

    List<Wallet> allWallets();

    Wallet addWallet(Wallet wallet) throws WalletExistForUserException;

    Wallet findWalletById(User user) throws WalletDoesNotExistException;
}
