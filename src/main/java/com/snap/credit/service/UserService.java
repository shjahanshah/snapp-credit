package com.snap.credit.service;

import com.snap.credit.domain.User;
import com.snap.credit.dto.request.UserChangePasswordRequestDTO;
import com.snap.credit.exception.user_service.UserCantPersistException;
import com.snap.credit.exception.user_service.UserChangePassWordEqualsException;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface UserService {

    User saveUser(User user) throws UserCantPersistException;

    User getUser(String username) throws UserDoesNotExistException;

    List<User> allUser();

    User editUser(User user) throws UserDoesNotExistException;

    User activateUser(String phone) throws UserDoesNotExistException;

    Boolean changePassword(UserChangePasswordRequestDTO dto) throws UserDoesNotExistException, UserChangePassWordEqualsException;

}
