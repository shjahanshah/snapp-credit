package com.snap.credit.service;

import com.snap.credit.domain.SnappAccount;
import com.snap.credit.domain.User;
import com.snap.credit.enums.SnappVenture;
import com.snap.credit.exception.account_service.wallet_service.AccountCantPersistException;
import com.snap.credit.exception.account_service.wallet_service.AccountDoesNotExistException;
import com.snap.credit.repository.SnappAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SnappAccountServiceImpl implements SnappAccountService {
    private Logger logger = LoggerFactory.getLogger(SnappAccountServiceImpl.class);

    private final SnappAccountRepository snappAccountRepository;

    public SnappAccountServiceImpl(SnappAccountRepository snappAccountRepository) {
        this.snappAccountRepository = snappAccountRepository;
    }

    @Override
    public List<SnappAccount> allAccount() {
        return snappAccountRepository.findAll();
    }


    @Override
    public List<SnappAccount> allBalanceByVenture(SnappVenture ventureName) {
        return snappAccountRepository.findAllByVenture(ventureName);
    }

    @Override
    public List<SnappAccount> allAccountsByPhone(String phone) {
        return snappAccountRepository.findAllByUserAccount_Mobile(phone);
    }

    @Override
    public SnappAccount editAccount(SnappAccount account, User user) throws AccountDoesNotExistException, AccountCantPersistException {
        SnappAccount userAccount = snappAccountRepository.findByUserAccount(user);
        if (userAccount == null) {
            throw new AccountDoesNotExistException(user.getMobile());
        }
        try {
            userAccount.setVenture(account.getVenture());
            userAccount.setBalance(account.getBalance());
            account = snappAccountRepository.save(account);
            logger.info("account saved!: {}", account);
            return account;
        } catch (Exception e) {
            throw new AccountCantPersistException(user.getMobile());
        }
    }

    @Override
    public SnappAccount addAccount(SnappAccount snappAccount) throws AccountCantPersistException {
        try {
            SnappAccount account = snappAccountRepository.save(snappAccount);
            if (account == null) {
                throw new AccountCantPersistException(snappAccount.getUserAccount().getMobile());
            }
            logger.info("account saved!: {}", account);
            return account;
        } catch (Exception e) {
            throw new AccountCantPersistException(snappAccount.getUserAccount().getMobile());
        }
    }
}
