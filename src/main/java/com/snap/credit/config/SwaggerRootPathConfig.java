package com.snap.credit.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.PathProvider;
import springfox.documentation.spring.web.paths.RelativePathProvider;

@Slf4j
@Configuration
public class SwaggerRootPathConfig {


    @Value("")
    private String applicationName;

    @Bean
    @Profile("dev")
    PathProvider defaultPathProvider() {
        return new RelativePathProvider(null) {
            @Override
            public String getApplicationBasePath() {
                return "/";
            }
        };
    }

    @Bean
    @Profile("!dev")
    PathProvider kubernetesPathProvider() {
        return new RelativePathProvider(null) {
            @Override
            public String getApplicationBasePath() {
                return "/".concat(applicationName);
            }
        };
    }
}