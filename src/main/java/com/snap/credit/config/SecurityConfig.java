package com.snap.credit.config;

import com.snap.credit.domain.User;
import com.snap.credit.filter.CustomAuthenticationFilter;
import com.snap.credit.filter.CustomAuthorizationFilter;
import com.snap.credit.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.ArrayList;
import java.util.Collection;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private Logger logger = LoggerFactory.getLogger(SecurityConfig.class);

    private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserRepository userRepository;

    @Autowired
    public SecurityConfig(@Lazy UserDetailsService userDetailsService, @Lazy BCryptPasswordEncoder bCryptPasswordEncoder, UserRepository userRepository) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRepository = userRepository;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean());
        customAuthenticationFilter.setFilterProcessesUrl("/api/login");
        http.csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().authorizeRequests().antMatchers(HttpMethod.GET, "/api/v1/user/**").hasAnyAuthority("ADMIN")
                .and().authorizeRequests().antMatchers(HttpMethod.GET, "/api/v1/role/**").hasAnyAuthority("ADMIN")
                .and().authorizeRequests().antMatchers(HttpMethod.GET, "/api/v1/user-role/**").hasAnyAuthority("ADMIN")
                .and().authorizeRequests().antMatchers("api/login/**").permitAll()
                .and().authorizeRequests().antMatchers("/v2/api-docs/**").permitAll()
                .and().authorizeRequests().antMatchers("/webjars/**").permitAll()
                .and().authorizeRequests().antMatchers("/health").permitAll()
                .and().authorizeRequests().antMatchers("/swagger-resources/**").permitAll()
                .and().authorizeRequests().antMatchers("/swagger-ui.html").permitAll()
                .and().authorizeRequests().anyRequest().authenticated()
                .and().addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilter(customAuthenticationFilter);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                User user = userRepository.findByMobile(username);
                if (user == null) {
                    logger.error("user not exist by username:{}", username);
                    throw new UsernameNotFoundException("user not exist by username: ".concat(username));
                }
                logger.info("user exist by username:{}", username);
                Collection<SimpleGrantedAuthority> authoritiesList = new ArrayList<>();
                user.getRoles().forEach(role -> {
                    authoritiesList.add(new SimpleGrantedAuthority(role.getRoleName()));
                });
                return new org.springframework.security.core.userdetails.User(username, user.getPassword(), authoritiesList);
            }
        };
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}