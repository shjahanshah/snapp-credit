package com.snap.credit.utils;

import com.snap.credit.domain.Role;
import com.snap.credit.domain.SnappAccount;
import com.snap.credit.domain.User;
import com.snap.credit.domain.Wallet;
import com.snap.credit.dto.response.AccountResponseDTO;
import com.snap.credit.dto.response.RoleResponseDTO;
import com.snap.credit.dto.response.UserResponseDTO;
import com.snap.credit.dto.response.WalletResponseDTO;

public class MappingResponse {
    public static UserResponseDTO userMapper(User user) {
        return UserResponseDTO.builder().active(user.getActive()).id(user.getId())
                .fullName(user.getFullName()).userName(user.getMobile()).build();
    }

    public static RoleResponseDTO roleMapper(Role role) {
        return RoleResponseDTO.builder().active(role.getActive()).id(role.getId())
                .roleName(role.getRoleName()).build();
    }

    public static WalletResponseDTO walletMapper(Wallet wallet) {
        return WalletResponseDTO.builder()
                .userFullName(wallet.getWalletUser().getFullName()).userId(wallet.getWalletUser().getId())
                .userPhone(wallet.getWalletUser().getMobile()).walletId(wallet.getWalletId()).build();
    }

    public static AccountResponseDTO accountMapper(SnappAccount account) {
        return AccountResponseDTO.builder()
                .userFullName(account.getUserAccount().getFullName()).userId(account.getUserAccount().getId())
                .userPhone(account.getUserAccount().getMobile()).walletId(account.getWalletHolder().getWalletId())
                .balance(account.getBalance()).venture(account.getVenture()).build();
    }
}
