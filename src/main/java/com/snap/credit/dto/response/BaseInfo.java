package com.snap.credit.dto.response;

import java.io.Serializable;
import java.util.Date;

public class BaseInfo implements Serializable {

    protected Date serverTime = new Date();

    protected String token;

    protected String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Date getServerTime() {
        return serverTime;
    }

    public void setServerTime(Date serverTime) {
        this.serverTime = serverTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
