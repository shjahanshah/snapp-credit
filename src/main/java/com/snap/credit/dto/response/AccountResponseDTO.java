package com.snap.credit.dto.response;

import com.snap.credit.enums.SnappVenture;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountResponseDTO {
    private Long id;
    private Long walletId;
    private Long userId;
    private String userFullName;
    private String userPhone;
    private SnappVenture venture;
    private Float balance;
}
