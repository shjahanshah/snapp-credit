package com.snap.credit.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserResponseDTO extends BaseInfo {
    private Long id;
    private String userName;
    private String fullName;
    private Boolean active;
}