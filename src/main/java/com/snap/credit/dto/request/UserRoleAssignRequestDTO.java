package com.snap.credit.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRoleAssignRequestDTO implements Serializable {
    @NotNull
    private String mobile;
    @NotNull
    private List<String> roles;
}