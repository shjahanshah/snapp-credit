package com.snap.credit.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestDTO implements Serializable {
    private Long id;
    @NotNull
    private String mobile;
    @NotNull
    private String fullName;
    @NotNull
    private String password;
    @Email
    private String email;
    private Boolean active = false;
}