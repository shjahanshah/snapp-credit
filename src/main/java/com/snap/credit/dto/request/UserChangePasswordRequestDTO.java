package com.snap.credit.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserChangePasswordRequestDTO implements Serializable {
    @NotNull
    private Long id;
    @NotNull
    private String mobile;
    @NotNull
    private String oldPassword;
    @NotNull
    private String newPassword;
}