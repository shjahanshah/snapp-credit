package com.snap.credit.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WalletRequestDTO {
    private Long walletId;
    private Long userId;
    private String userFullName;
    private String userPhone;
}
