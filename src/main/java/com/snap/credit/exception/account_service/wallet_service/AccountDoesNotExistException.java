package com.snap.credit.exception.account_service.wallet_service;

public class AccountDoesNotExistException extends Exception {

    public AccountDoesNotExistException(String phone) {
        super("Wallet with user:" + phone + " does not exist");
    }
}