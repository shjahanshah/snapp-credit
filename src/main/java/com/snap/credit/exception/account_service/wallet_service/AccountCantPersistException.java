package com.snap.credit.exception.account_service.wallet_service;

public class AccountCantPersistException extends Exception {

    public AccountCantPersistException(String phone) {
        super("account with phone:" + phone + " can not persist");
    }
}