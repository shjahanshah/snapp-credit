package com.snap.credit.exception.role_service.user_service;

public class RoleNotExistExceptionException extends Exception {

    public RoleNotExistExceptionException(String role) {
        super("Role with name:" + role + " not exist");
    }
}