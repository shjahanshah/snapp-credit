package com.snap.credit.exception.user_service;

public class UserChangePassWordEqualsException extends Exception {

    public UserChangePassWordEqualsException(String phone) {
        super("new password and old password with phone:" + phone + " are equals");
    }
}