package com.snap.credit.exception.user_service;

public class UserDoesNotExistException extends Exception {

    public UserDoesNotExistException(String phone) {
        super("User with phone:" + phone + " does not exist");
    }
}
