package com.snap.credit.exception.user_service;

public class UserCantPersistException extends Exception {

    public UserCantPersistException(String phone) {
        super("User with phone:" + phone + " can not persist");
    }
}