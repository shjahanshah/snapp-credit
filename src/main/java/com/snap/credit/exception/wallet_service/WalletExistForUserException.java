package com.snap.credit.exception.wallet_service;

public class WalletExistForUserException extends Exception {

    public WalletExistForUserException(String phone) {
        super("wallet is exist for user:" + phone);
    }
}