package com.snap.credit.exception.wallet_service;

public class WalletDoesNotExistException extends Exception {

    public WalletDoesNotExistException(String phone) {
        super("Wallet with user:" + phone + " does not exist");
    }
}