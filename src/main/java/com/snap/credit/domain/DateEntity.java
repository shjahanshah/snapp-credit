package com.snap.credit.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
public class DateEntity implements Serializable {
    @JsonIgnore
    @Version
    protected Long version = 0L;

    @JsonIgnore
    @Transient
    private int rownumber;

    @CreatedBy
    @JsonIgnore
    @Column(updatable = false)
    private String createdBy = "";

    @LastModifiedBy
    @JsonIgnore
    private String updatedBy = "";

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    @Column(updatable = false)
    private Date createdAt;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date updatedAt;

}
