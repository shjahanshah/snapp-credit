package com.snap.credit.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ROLE")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Role extends DateEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "role_name")
    @NotNull
    private String roleName;

    @Column(name = "active")
    @NotNull
    private Boolean active;

    @ManyToMany(mappedBy = "roles")
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private List<User> users = new ArrayList<>();
}

