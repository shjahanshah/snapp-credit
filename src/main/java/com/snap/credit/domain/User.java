package com.snap.credit.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "USER")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends DateEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "full_name")
    @NotNull
    private String fullName;

    @Column(name = "mobile", unique = true)
    @NotNull
    private String mobile;

    @Column(name = "password")
    @NotNull
    private String password;

    @Column(name = "email", nullable = true)
    @Email
    private String email;

    @Column(name = "active")
    @NotNull
    private Boolean active;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "USER_ROLES",
            joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<Role> roles = new ArrayList<>();

    @OneToOne(mappedBy = "walletUser")
    private Wallet wallet;

    @OneToMany(mappedBy = "userAccount")
    private List<SnappAccount> snappAccounts;
}
