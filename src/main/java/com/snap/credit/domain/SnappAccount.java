package com.snap.credit.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.snap.credit.enums.SnappVenture;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SNAPP_ACCOUNT")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SnappAccount extends DateEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "balance")
    private float balance;

    @ManyToOne
    @JoinColumn(name = "user_id",referencedColumnName = "id",nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private User userAccount;

    @Column(name = "venture")
    @Enumerated(EnumType.STRING)
    private SnappVenture venture;

    @ManyToOne
    @JoinColumn(name = "wallet_id",referencedColumnName = "id",nullable = false)
    @JsonIgnore
    private Wallet walletHolder;

   /* @OneToMany(mappedBy = "transactionFromAccount")
    private List<BankTransaction> bankTransactions;
    */
}
