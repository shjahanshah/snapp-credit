package com.snap.credit.decorator;

import com.snap.credit.dto.request.UserChangePasswordRequestDTO;
import com.snap.credit.dto.request.UserRequestDTO;
import com.snap.credit.dto.response.UserResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserDecorator {

    ResponseEntity<?> getUser(String username);

    ResponseEntity<List<UserResponseDTO>> allUser();

    ResponseEntity<?> addUser(UserRequestDTO dto);

    ResponseEntity<?> editUser(UserRequestDTO dto);

    ResponseEntity<?> changePassword(UserChangePasswordRequestDTO dto);

    ResponseEntity<?> activateUser(String phone);

}