package com.snap.credit.decorator;

import com.snap.credit.domain.User;
import com.snap.credit.dto.request.WalletRequestDTO;
import com.snap.credit.dto.response.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WalletDecorator {

    ResponseEntity<List<WalletResponseDTO>> allWallets();

    ResponseEntity<?> addWallet(WalletRequestDTO dto);
}