package com.snap.credit.decorator;

import com.snap.credit.domain.User;
import com.snap.credit.dto.request.UserRoleAssignRequestDTO;
import com.snap.credit.dto.response.UserResponseDTO;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import com.snap.credit.service.UserRoleService;
import com.snap.credit.service.UserServiceImpl;
import com.snap.credit.utils.MappingResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
@Service
public class UserRoleDecoratorImpl implements UserRoleDecorator {
    private Logger logger = LoggerFactory.getLogger(UserRoleDecoratorImpl.class);

    private final UserRoleService userRoleService;

    public UserRoleDecoratorImpl(UserRoleService userRoleService) {
        this.userRoleService = userRoleService;
    }

    @Override
    public ResponseEntity<?> assignRole(UserRoleAssignRequestDTO dto) {
        User user = null;
        if (dto.getMobile().equals("")||dto.getRoles().isEmpty()){
            return new ResponseEntity<>("mobile or roles are empty", HttpStatus.BAD_REQUEST);
        }
        try {
            user = userRoleService.assignRole(dto.getMobile(), dto.getRoles());
            return new ResponseEntity<>(MappingResponse.userMapper(user), HttpStatus.OK);
        } catch (UserDoesNotExistException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
}
