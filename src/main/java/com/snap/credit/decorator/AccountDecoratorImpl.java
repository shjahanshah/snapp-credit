package com.snap.credit.decorator;

import com.snap.credit.domain.SnappAccount;
import com.snap.credit.domain.User;
import com.snap.credit.domain.Wallet;
import com.snap.credit.dto.request.AccountRequestDTO;
import com.snap.credit.dto.response.AccountResponseDTO;
import com.snap.credit.enums.SnappVenture;
import com.snap.credit.exception.account_service.wallet_service.AccountCantPersistException;
import com.snap.credit.exception.account_service.wallet_service.AccountDoesNotExistException;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import com.snap.credit.exception.wallet_service.WalletDoesNotExistException;
import com.snap.credit.service.SnappAccountService;
import com.snap.credit.service.UserService;
import com.snap.credit.service.WalletService;
import com.snap.credit.utils.MappingResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountDecoratorImpl implements AccountDecorator {
    private Logger logger = LoggerFactory.getLogger(AccountDecoratorImpl.class);

    private final SnappAccountService snappAccountService;
    private final UserService userService;
    private final WalletService walletService;

    public AccountDecoratorImpl(SnappAccountService snappAccountService, UserService userService, WalletService walletService) {
        this.snappAccountService = snappAccountService;
        this.userService = userService;
        this.walletService = walletService;
    }

    @Override
    public ResponseEntity<List<AccountResponseDTO>> allAccount() {
        List<SnappAccount> snappAccounts = snappAccountService.allAccount();
        List<AccountResponseDTO> accountRequestDTOS = new ArrayList<>();
        snappAccounts.forEach(account -> {
            accountRequestDTOS.add(MappingResponse.accountMapper(account));
        });
        return new ResponseEntity<>(accountRequestDTOS, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> addAccount(AccountRequestDTO dto) {
        User user = null;
        Wallet wallet = null;
        try {
            user = userService.getUser(dto.getUserPhone());
        } catch (UserDoesNotExistException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NO_CONTENT);
        }
        try {
            wallet = walletService.findWalletById(user);
        } catch (WalletDoesNotExistException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NO_CONTENT);
        }
        SnappAccount account = SnappAccount.builder().userAccount(user).balance(dto.getBalance()).venture(dto.getVenture())
                .walletHolder(wallet).build();
        try {
            return new ResponseEntity<>(MappingResponse.accountMapper(snappAccountService.addAccount(account)), HttpStatus.OK);
        } catch (AccountCantPersistException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Override
    public ResponseEntity<?> editAccount(AccountRequestDTO dto) {
        User user = null;
        SnappAccount editAccount = null;
        try {
            user = userService.getUser(dto.getUserPhone());
        } catch (UserDoesNotExistException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        try {
            SnappAccount account = SnappAccount.builder().userAccount(user).balance(dto.getBalance())
                    .venture(dto.getVenture()).walletHolder(user.getWallet()).build();
            editAccount = snappAccountService.editAccount(account, user);
            return new ResponseEntity<>(MappingResponse.accountMapper(editAccount), HttpStatus.OK);
        } catch (AccountDoesNotExistException | AccountCantPersistException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<AccountResponseDTO>> allBalanceByVenture(SnappVenture ventureName) {
        List<SnappAccount> snappAccounts = snappAccountService.allBalanceByVenture(ventureName);
        List<AccountResponseDTO> accountRequestDTOS = new ArrayList<>();
        snappAccounts.forEach(account -> {
            accountRequestDTOS.add(MappingResponse.accountMapper(account));
        });
        return new ResponseEntity<>(accountRequestDTOS, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<AccountResponseDTO>> allAccountsByPhone(String phone) {
        List<SnappAccount> snappAccounts = snappAccountService.allAccountsByPhone(phone);
        List<AccountResponseDTO> accountRequestDTOS = new ArrayList<>();
        snappAccounts.forEach(account -> {
            accountRequestDTOS.add(MappingResponse.accountMapper(account));
        });
        return new ResponseEntity<>(accountRequestDTOS, HttpStatus.OK);
    }
}
