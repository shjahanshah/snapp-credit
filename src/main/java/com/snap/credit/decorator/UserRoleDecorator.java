package com.snap.credit.decorator;

import com.snap.credit.dto.request.UserRoleAssignRequestDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public interface UserRoleDecorator {
    ResponseEntity<?> assignRole(UserRoleAssignRequestDTO dto);
}