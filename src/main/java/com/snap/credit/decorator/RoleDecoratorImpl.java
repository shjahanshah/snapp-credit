package com.snap.credit.decorator;

import com.snap.credit.domain.Role;
import com.snap.credit.dto.response.RoleResponseDTO;
import com.snap.credit.exception.role_service.user_service.RoleNotExistExceptionException;
import com.snap.credit.service.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.snap.credit.utils.MappingResponse.roleMapper;

@Service
public class RoleDecoratorImpl implements RoleDecorator {
    private Logger logger = LoggerFactory.getLogger(RoleDecoratorImpl.class);

    private final RoleService roleService;

    public RoleDecoratorImpl(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public ResponseEntity<List<RoleResponseDTO>> allRoles() {
        List<Role> roles = roleService.allRole();
        List<RoleResponseDTO> responseDTOS = new ArrayList<>();
        roles.forEach(role -> {
            responseDTOS.add(roleMapper(role));
        });
        return new ResponseEntity<>(responseDTOS, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> activateRole(String roleName) {
        Role role = null;
        try {
            role = roleService.activateRole(roleName);
        } catch (RoleNotExistExceptionException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(roleMapper(role), HttpStatus.OK);
    }
}
