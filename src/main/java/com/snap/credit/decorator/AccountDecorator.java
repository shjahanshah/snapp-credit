package com.snap.credit.decorator;

import com.snap.credit.dto.request.AccountRequestDTO;
import com.snap.credit.dto.response.AccountResponseDTO;
import com.snap.credit.dto.response.RoleResponseDTO;
import com.snap.credit.enums.SnappVenture;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AccountDecorator {

    ResponseEntity<List<AccountResponseDTO>> allAccount();

    ResponseEntity<?> addAccount(AccountRequestDTO dto);

    ResponseEntity<?> editAccount(AccountRequestDTO dto);

    ResponseEntity<List<AccountResponseDTO>> allBalanceByVenture(SnappVenture ventureName);

    ResponseEntity<List<AccountResponseDTO>> allAccountsByPhone(String phone);

}
