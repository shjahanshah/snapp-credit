package com.snap.credit.decorator;

import com.snap.credit.domain.User;
import com.snap.credit.dto.request.UserChangePasswordRequestDTO;
import com.snap.credit.dto.request.UserRequestDTO;
import com.snap.credit.dto.response.UserResponseDTO;
import com.snap.credit.exception.user_service.UserCantPersistException;
import com.snap.credit.exception.user_service.UserChangePassWordEqualsException;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import com.snap.credit.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.snap.credit.utils.MappingResponse.userMapper;

@Service
public class UserDecoratorImpl implements UserDecorator {
    private Logger logger = LoggerFactory.getLogger(UserDecoratorImpl.class);

    private final UserService userService;

    public UserDecoratorImpl(UserService userService) {
        this.userService = userService;
    }

    /**
     * get user by user name
     **/
    @Override
    public ResponseEntity<?> getUser(String username) {
        User user = null;
        if (username.equals("")) {
            return notValidRequest();
        }
        try {
            user = userService.getUser(username);
            return new ResponseEntity<>(userMapper(user), HttpStatus.OK);
        } catch (UserDoesNotExistException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }

    }

    /**
     * show all users
     **/
    @Override
    public ResponseEntity<?> addUser(UserRequestDTO dto) {
        if (userIsValidate(dto)) {
            User user = convertDtoToUser(dto);
            User saveUser = null;
            try {
                saveUser = userService.saveUser(user);
                return new ResponseEntity<>(userMapper(saveUser), HttpStatus.OK);
            } catch (UserCantPersistException e) {
                logger.error(e.getMessage());
                return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
            }

        }
        return notValidRequest();
    }

    private ResponseEntity<?> notValidRequest() {
        return new ResponseEntity<>("data is not valid", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<?> editUser(UserRequestDTO dto) {
        if (userIsValidate(dto)) {
            User user = null;
            try {
                user = userService.editUser(convertDtoToUser(dto));
                return new ResponseEntity<>(userMapper(user), HttpStatus.OK);
            } catch (UserDoesNotExistException e) {
                logger.error(e.getMessage());
                return new ResponseEntity<>(e.getMessage(), HttpStatus.NO_CONTENT);
            }
        }
        return notValidRequest();
    }

    @Override
    public ResponseEntity<?> changePassword(UserChangePasswordRequestDTO dto) {
        if (dto.getId() != null && !dto.getMobile().equals("")
                && !dto.getOldPassword().equals("") && !dto.getNewPassword().equals("")) {
            Boolean changed = null;
            try {
                changed = userService.changePassword(dto);
                return new ResponseEntity<>(changed, HttpStatus.OK);
            } catch (UserDoesNotExistException | UserChangePassWordEqualsException e) {
                logger.error(e.getMessage());
                return new ResponseEntity<>(e.getMessage(), HttpStatus.NO_CONTENT);
            }
        }
        return notValidRequest();
    }

    @Override
    public ResponseEntity<?> activateUser(String phone) {
        if (!phone.equals("") && phone.length() == 11) {
            User user = null;
            try {
                user = userService.activateUser(phone);
                return new ResponseEntity<>(userMapper(user), HttpStatus.OK);
            } catch (UserDoesNotExistException e) {
                logger.error(e.getMessage());
                return new ResponseEntity<>(e.getMessage(), HttpStatus.NO_CONTENT);
            }
        }
        return notValidRequest();
    }


    /**
     * show all users
     **/
    @Override
    public ResponseEntity<List<UserResponseDTO>> allUser() {
        List<UserResponseDTO> responses = new ArrayList<>();
        userService.allUser().forEach(user -> {
            responses.add(userMapper(user));
        });
        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    /**
     * validate user
     **/
    private boolean userIsValidate(UserRequestDTO user) {
        return !user.getFullName().equals("") && !user.getPassword().equals("") && !user.getMobile().equals("");
    }

    /**
     * user to user dto mapper
     **/


    private User convertDtoToUser(UserRequestDTO dto) {
        return User.builder().id(dto.getId()).active(dto.getActive()).email(dto.getEmail()).fullName(dto.getFullName())
                .mobile(dto.getMobile()).password(dto.getPassword()).build();
    }

}
