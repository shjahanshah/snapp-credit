package com.snap.credit.decorator;

import com.snap.credit.dto.response.RoleResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleDecorator {
    ResponseEntity<List<RoleResponseDTO>> allRoles();

    ResponseEntity<?> activateRole(String roleName);
}
