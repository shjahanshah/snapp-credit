package com.snap.credit.decorator;

import com.snap.credit.domain.User;
import com.snap.credit.domain.Wallet;
import com.snap.credit.dto.request.WalletRequestDTO;
import com.snap.credit.dto.response.WalletResponseDTO;
import com.snap.credit.exception.user_service.UserDoesNotExistException;
import com.snap.credit.exception.wallet_service.WalletExistForUserException;
import com.snap.credit.service.UserService;
import com.snap.credit.service.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.snap.credit.utils.MappingResponse.walletMapper;

@Service
public class WalletDecoratorImpl implements WalletDecorator {
    private Logger logger = LoggerFactory.getLogger(WalletDecoratorImpl.class);

    private final WalletService walletService;
    private final UserService userService;

    public WalletDecoratorImpl(WalletService walletService, UserService userService) {
        this.walletService = walletService;
        this.userService = userService;
    }

    @Override
    public ResponseEntity<List<WalletResponseDTO>> allWallets() {
        List<Wallet> wallets = walletService.allWallets();
        List<WalletResponseDTO> responseDTOS = new ArrayList<>();
        wallets.forEach(wallet -> {
            responseDTOS.add(walletMapper(wallet));
        });
        return new ResponseEntity<>(responseDTOS, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> addWallet(WalletRequestDTO dto) {
        User user = null;
        try {
            user = userService.getUser(dto.getUserPhone());
        } catch (UserDoesNotExistException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        Wallet wallet = Wallet.builder().walletUser(user).build();
        try {
            wallet = walletService.addWallet(wallet);
            return new ResponseEntity<>(walletMapper(wallet), HttpStatus.OK);
        } catch (WalletExistForUserException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }
}
