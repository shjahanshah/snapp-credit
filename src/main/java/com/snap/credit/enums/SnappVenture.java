package com.snap.credit.enums;

public enum SnappVenture {
    SNAPP_CAB, SNAPP_FOOD, SNAPP_MARKET, SNAPP_DOCTOR;
}