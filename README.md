# Snapp Credit Project


This project is about  *Building a Java credit to centralize wallet for snapp venture and OAuth security Implementation*

This is a spring boot application within, the MySQL database. I am using JPA(Java Persistence API) to interact with the database.



## Tech Stack

* Java 8 
* Spring Boot
* Spring Security
* Mysql
* Hibernate
* JPA
* Liquibase
* Docker
* Docker-Compose



## UML Diagram

Following UML diagram indicates the database tables and thier interaction which I am using.

![uml diagram](/uploads/74ed8e820551ac2299f35d482b314b00/uml.png)


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/shjahanshah/snapp-credit.git
```

Go to the project directory

```bash
  cd snapp-credit
```

Install project

```bash
  mvn clean compile package
```



## Run and Deployment

To run and deploy this project run 

```bash
  docker-compose -f docker-compose.yml up
```

* Now navigate to http://localhost:8081/swagger-ui.html/ . You would see list of APIs in swagger page

**Congratulations. You have successfully run the project.**
## Screenshots

![swagger screenshot](/uploads/976bf5d72ef8c19ed5c38d503026048d/swagger.PNG)



## Login To project

To start login use this curl:
```bash
curl --location --request POST 'http://localhost:8081/api/login' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Accept: application/json' \
--header 'Application-Name: SNAPP_CREDIT' \
--data-urlencode 'username=09123456789' \
--data-urlencode 'password=admin'
```
and response is:

```json
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwOTEyMzQ1Njc4OSIsInJvbGVzIjpbIkFETUlOIl0sImlzcyI6Ii9hcGkvbG9naW4iLCJleHAiOjE2NDU2NDU0MDl9.NvMbkH72OIJjBjbnyDCw-UFgzbq6WpkhV63-vMAkP7U",
    "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwOTEyMzQ1Njc4OSIsInJvbGVzIjpbIkFETUlOIl0sImlzcyI6Ii9hcGkvbG9naW4iLCJleHAiOjE2NDU2NDg0MDl9.nOub7qDHk-_Qy-T8YjhWW6vLtmz4tXd5U8KEgHYq3TE"
}
```
## API Reference

#### Inside Swagger you can see 5 base API

```http
  GET /api/items
```

| API                             |            USAGE                      |
| :--------                       |      :-------------------------       |
| `role-rest-controller`          | See all roles and activate role       |
| `snapp-account-rest-controller` | See all account, add account and etc. |
| `user-rest-controller`          |         See user managment apis       |
| `user-role-rest-controller`     | See api for role assignment           |
| `wallet-rest-controller`        | See all wallets and add user wallet   |

#### role-rest-controller

```http
  PATCH /api/v1/role/activate-role/{roleName}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `roleName`| `string` | **Required**                      |

```http
  GET /api/v1/role/all
```
#### user-rest-controller

```http
  GET /api/v1/user/all
```

```http
  GET /api/v1/user/{userName}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `userName`| `string` | **Required**    phone number      |

```http
  POST /api/v1/user/add
```

| Parameter | Type             | Description                       |
| :-------- | :-------         | :-------------------------------- |
| `UserRequestDTO `| `DTO Object` | **Required**       |

```http
  PUT /api/v1/user/edit
```

| Parameter | Type             | Description                       |
| :-------- | :-------         | :-------------------------------- |
| `UserRequestDTO`| `DTO Object` | **Required**         |


```http
  PUT /api/v1/user/change-password
```

| Parameter | Type                           |            Description                       |
| :-------- | :-------           |   :-------------------------------- |
| `UserChangePasswordRequestDTO `| `DTO Object` | **Required**          |


```http
  PATCH /activate-user/{phone}
```

| Parameter | Type     |            Description                       |
| :-------- | :------- |            :-------------------------------- |
| `phone   `| `String` | **Required**              |

#### user-role-rest-controller

```http
  PUT /api/v1/user-role/assign-role
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `UserRoleAssignRequestDTO`| `DTO Object` | **Required**                      |


#### snapp-account-rest-controller


```http
  GET /api/v1/account/all
```

```http
  GET /api/v1/account/all/{phone}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `phone`| `String` | **Required**                      |


```http
  POST /api/v1/account/add
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `AccountRequestDTO`| `DTO Object` | **Required**                      |


```http
  PUT /api/v1/account/edit
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `AccountRequestDTO`| `DTO Object` | **Required**                      |

```http
  GET /api/v1/account/all/balance/{ventureName}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `ventureName`| `SnappVenture Enums` | **Required**                      |

#### wallet-rest-controller

```http
  GET /api/v1/wallet/all
```

```http
  POST /api/v1/wallet/add-wallet
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `WalletRequestDTO`| `DTO Object` | **Required**                      |

You can use the above APIs for managing snapp credit application


#### ***transaction-rest-controller [ Coming Soon]***



## Additional Information

This application was developed based on docker and docker-compose for better and easier deployment. in addition, I added JaCoCo for the integration testing and you can see the result in the below image
Thank you :)

![integration_test](/uploads/7193c3be62a4b4f6738aea872fac7f71/integration_test.PNG)
## Authors

- [@shjahanshah](https://gitlab.com/shjahanshah)

