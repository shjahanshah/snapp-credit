#USED FOR MULTI STAGE#
#FROM maven:3.6.0-jdk-11-slim AS build
#RUN mvn clean package
##########################################
FROM openjdk:8
ADD target/snapp_credit-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8081
ENTRYPOINT ["java","-jar","/app.jar"]
